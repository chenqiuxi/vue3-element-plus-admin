# vue3-element-plus-admin
## 启动项目
  npm run serve
  npm run api


## npm run server服务需要全局安装nodemon
npm install -g nodemon


### 1.语言国际化 i18n
#### 原理
1.通过一个变量控制语言环境；
2.所有语言环境下的数据源要预先定义好；
3.通过一个方法来获取当前语言下指定属性的值；
4.该值即为国际化下展示值；

##### 依赖
cnpm install vue-i18n@next -S

##### 使用
1.注册 message 数据源；
2.创建locale 语言变量；
3.初始化 i18你实例;
4.注册i18n实例；


### 2.换肤功能

#### 依赖
cnpm i rgb-hex  css-color-function -D
#### 原理
核心原理就是：通过修改 scss 变量 的形式修改主题色完成主题变更

修改element-plus的原理：
1.获取当前 element-plus 的所有样式；
2.找到我们降妖替换的样式部分，通过正则完成替换；
3，把替换后的样式写入到style标签中，利用样式优先级的特性，替代固有样式；


### 3.全屏功能
#### 依赖
cnpm i screenfull@5.1.0 -D
##### 原理
浏览器默认提供了api
1.Document.exitFullscreen()：该方法用于请求从全屏模式切换到窗口模式；
2.Element.requestFullscreen()：该方法用于请求浏览器将特定元素置为全屏模式；

### 4.headerSearch模糊搜索
#### 依赖
cnpm i fuse.js@6.4.6 -S

### 5.guide引导功能
#### 依赖
cnpm i driver.js@0.9.8 -S

### 6.excel的导入导出功能
cnpm i xlsx@0.17.0 file-saver@2.0.5 -S

### 7.打印功能
cnpm i vue3-print-nb@0.1.4 -S

### 8.按钮权限指令
<el-button type="primary" v-permission="['importExcel']">导入</el-button>

### 9.表格拖拽
cnpm i sortablejs@1.14.0 -S

### 10.markdown + prismjs(代码高亮工具)
cnpm i @kangc/v-md-editor@next prismjs -S
文档地址：https://ckang1229.gitee.io/vue-markdown-editor/zh/examples/base-editor.html#%E5%BC%95%E5%85%A5