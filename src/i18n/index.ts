import { createI18n } from 'vue-i18n'
import zhLocale from './lang/zh'
import enLocale from './lang/en'
import { LANG } from '@/contant'
import { getItem } from '@/utils/storage'

const messages = {
  zh: {
    msg: { ...zhLocale }
  },
  en: {
    msg: { ...enLocale }
  }
}

function getLanguage() {
  return getItem(LANG) || 'en'
}

const i18n = createI18n({
  // 使用 Composition API 模式，则需要将其设置为false
  legacy: false,
  // 全局注入 $t 函数
  globalInjection: true,
  locale: getLanguage(),
  fallbackLocale: 'zh-cn',
  messages
})
export default i18n
