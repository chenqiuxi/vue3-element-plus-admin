import { post, get } from '@/utils/request'

// 登录
export const login = async (data: any): Promise<any> =>
  await post('/sys/login', data)

// 获取用户信息
export const getUserInfo = async (): Promise<any> => await get('/sys/profile')

// 用户退出
export const logout = async (): Promise<any> => await get('/sys/logout')
