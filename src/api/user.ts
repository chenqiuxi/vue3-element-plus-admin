import { get } from '@/utils/request'

// 获取项目功能数据
export const userFeature = async (): Promise<any> => {
  const { data } = await get('/user/feature')
  return data
}

// 获取章节数据
export const userChapter = async (): Promise<any> => {
  const { data } = await get('/user/chapter')
  return data
}

// 获取员工管理数据
export const userManageList = async (): Promise<any> => {
  const { data } = await get('/user/manage/list')
  return data
}
// 获取员工管理详情
export const userDetail = async (): Promise<any> => {
  const { data } = await get(`/user/detail/1`)
  return data
}

// 获取角色列表
export const roleList = async (): Promise<any> => {
  const { data } = await get(`/role/list`)
  return data
}

// 获取权限列表
export const permissionList = async (): Promise<any> => {
  const { data } = await get(`/permission/list`)
  return data
}
