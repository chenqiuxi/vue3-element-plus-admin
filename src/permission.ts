import { useUserStore } from '@/store'
import router from '@/router'

const whiteRouter = ['/login']

router.beforeEach((to, from, next) => {
  const userStore = useUserStore()
  // 1.用户已登录，则不允许进入login
  if (userStore.token) {
    if (to.path === '/login') {
      next('/admin')
    } else {
      // 判断用户信息是否存在，不存在则获取
      if (!userStore.hasUserInfo) {
        userStore.getUserInfo()
      }
      next()
    }
  } else {
    // 2.用户未登录，只允许进如login
    if (whiteRouter.indexOf(to.path) > -1) {
      next()
    } else {
      next('/login')
    }
  }
})
