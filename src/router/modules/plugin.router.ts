import Layout from '@/views/layout/index.vue'

export default {
  path: '/admin/plugin',
  component: Layout,
  redirect: '/admin/plugin/import',
  meta: { title: '插件页面', icon: 'router-plugin' },
  children: [
    {
      path: '/admin/plugin/import',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "pages-plugin" */ '@/views/pages-plugin/import/index.vue'
        ),
      meta: { title: 'excel导入', icon: 'router-excel' }
    },
    {
      path: '/admin/plugin/export',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "pages-plugin" */ '@/views/pages-plugin/export/index.vue'
        ),
      meta: { title: 'excel导出', icon: 'router-excel' }
    }
  ]
}
