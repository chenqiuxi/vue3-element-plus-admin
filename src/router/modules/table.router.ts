import Layout from '@/views/layout/index.vue'

export default {
  path: '/admin/function',
  component: Layout,
  redirect: '/admin/function/dynamic',
  meta: { title: '超级表格', icon: 'article' },
  children: [
    {
      path: '/admin/table/dynamic',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "article-ranking" */ '@/views/table/dynamic/index.vue'
        ),
      meta: {
        title: '动态表格',
        icon: 'article-ranking'
      }
    },
    {
      path: '/admin/table/sortable',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "article-ranking" */ '@/views/table/sortable/index.vue'
        ),
      meta: {
        title: '排序表格',
        icon: 'article-ranking'
      }
    },
    {
      path: '/admin/table/markdown',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "article-ranking" */ '@/views/table/markdown/index.vue'
        ),
      meta: {
        title: 'markdown文档',
        icon: 'article-ranking'
      }
    }
  ]
}
