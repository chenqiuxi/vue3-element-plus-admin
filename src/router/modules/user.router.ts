import Layout from '@/views/layout/index.vue'

export default {
  path: '/admin/user',
  component: Layout,
  redirect: '/admin/user/manage',
  meta: { title: '用户管理', icon: 'personnel' },
  children: [
    {
      path: '/admin/user/manage',
      name: 'userManage',
      component: (): Promise<typeof import('*.vue')> =>
        import('@/views/user/user-manage/index.vue'),
      meta: { title: '员工管理', icon: 'personnel-manage' }
    },
    {
      path: '/admin/user/role',
      name: 'roleList',
      component: (): Promise<typeof import('*.vue')> =>
        import('@/views/user/role-list/index.vue'),
      meta: { title: '角色列表', icon: 'role' }
    },
    {
      path: '/admin/user/permission',
      name: 'permissionList',
      component: (): Promise<typeof import('*.vue')> =>
        import('@/views/user/permission-list/index.vue'),
      meta: { title: '权限列表', icon: 'permission' }
    },
    {
      path: '/admin/user/info/:id',
      name: 'userInfo',
      component: (): Promise<typeof import('*.vue')> =>
        import('@/views/user/user-info/index.vue'),
      meta: {
        title: '用户信息',
        icon: 'user-info',
        role: ['view', 'edit', 'delete']
      }
    }
  ]
}
