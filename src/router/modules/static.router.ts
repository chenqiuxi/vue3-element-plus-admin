import Layout from '@/views/layout/index.vue'

export default {
  path: '/admin',
  redirect: '/admin/pages/profile',
  meta: { title: '页面管理', icon: 'router-pages' },
  component: Layout,
  children: [
    {
      path: '/admin/pages/profile',
      name: 'profile',
      component: () => import('@/views/pages/profile/index.vue'),
      meta: { title: '个人中心' }
    },
    {
      path: '/admin/pages/404',
      name: '404',
      meta: { title: '404' },
      component: () => import('@/views/pages/error-page/404.vue')
    },
    {
      path: '/admin/pages/401',
      name: '401',
      meta: { title: '401' },
      component: () => import('@/views/pages/error-page/401.vue')
    }
  ]
}
