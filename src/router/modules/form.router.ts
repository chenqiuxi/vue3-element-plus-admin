import Layout from '@/views/layout/index.vue'

export default {
  path: '/admin/form',
  component: Layout,
  redirect: '/admin/form/index',
  meta: { title: '超级表单', icon: 'article' },
  children: [
    {
      path: '/admin/form/index',
      component: (): Promise<typeof import('*.vue')> =>
        import(
          /* webpackChunkName: "article-ranking" */ '@/views/form/index/index.vue'
        ),
      meta: {
        title: '配置表单',
        icon: 'form'
      }
    }
  ]
}
