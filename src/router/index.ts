import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

import userRoutes from './modules/user.router'
import tableRoutes from './modules/table.router'
import staticRouter from './modules/static.router'
import pluginRouter from './modules/plugin.router'
import formRouter from './modules/form.router'

// 私有路由表
export const privateRoutes: RouteRecordRaw[] = [
  userRoutes,
  tableRoutes,
  staticRouter,
  pluginRouter,
  formRouter
]
// 公开路由表
export const publicRoutes: RouteRecordRaw[] = [
  {
    path: '/',
    meta: { title: '管理系统' },
    redirect: '/login'
  },
  {
    path: '/login',
    meta: { title: '登录', icon: 'el-icon-user' },
    component: () =>
      import(/* webpackChunkName:"login" */ '@/views/login/index.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: [...publicRoutes, ...privateRoutes]
})

export default router
