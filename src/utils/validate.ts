// 判断svg 图标是否为外部资源
export const isExternal = (path: string): boolean =>
  /^(https?:|mailto:|tel:)/.test(path)

// 判断空
export const isNull = (value: unknown): boolean => {
  if (!value) return true
  if (JSON.stringify(value) === '{}') return true
  if (JSON.stringify(value) === '[]') return true
}
