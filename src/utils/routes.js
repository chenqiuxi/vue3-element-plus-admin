import path from 'path'
import { isNull } from './validate'

// 获取所有的子集路由
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getChildrenRoutes = (routes) => {
  const result = []
  routes.forEach(
    (route) => route.children && route.children.length > 0 && result.push(route)
  )
  return result
}

// 处理脱离层级的路由
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const filterRoutes = (routes) => {
  // 所有的子集路由
  const childrenRoutes = getChildrenRoutes(routes)
  // 根据子集路由进行查重操作
  return routes.filter((route) => {
    // 根据route在children中进行查重，把所有重复路由表剔除
    return !childrenRoutes.find(
      (childrenRoute) => childrenRoute.path === route.path
    )
  })
}

// 根据routes数据，返回对应的menu规则数据
export const generateMenu = (routes, basePath) => {
  let result = []
  // 不满足该条件  meta && meta.title  && meta.icon 的数据不应该存在
  routes.forEach((item) => {
    // 不存在children && 不存在meta 直接return
    if (isNull(item.children) && isNull(item.meta)) return

    // 存在children，不存在meta，递归调用generateMenu
    if (!isNull(item.children) && isNull(item.meta)) {
      result.push(...generateMenu(item.children))
      return
    }

    // 不存在children，存在meta
    // 因为最终的menu需要进行跳转，此时我们需要合并path
    const routePath = path.resolve(basePath, item.path)
    // 路由分离后，可能存在同名父路由的情况
    let route = result.find((item) => item.path === routePath)
    // 如果路由没有加入到了result中则加入
    if (!route) {
      route = {
        ...item,
        path: routePath,
        children: []
      }
      // icon && title
      if (route.meta.icon && route.meta.title) {
        result.push(route)
      }
    }

    // 存在children，存在meta
    if (!isNull(item.children)) {
      route.children.push(...generateMenu(item.children, route.path))
    }
  })

  return result
}
