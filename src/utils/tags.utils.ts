// 白名单的路径是全路径
const whitelist = ['/login', '/404', '/401']

// 是否需要被缓存
export const isTags = (path) => !whitelist.includes(path)
