import formula from '@/contant/formula.json'
import rgbHex from 'rgb-hex'
import color from 'css-color-function'
import axios from 'axios'

// 把生成的样式表写入到style中
export const writeNewStyle = (elNewStyle) => {
  const style = document.createElement('style')
  style.innerText = elNewStyle
  document.head.appendChild(style)
}

// 根据主题色，生成最新的样式表
export const generateNewStyle = async (primaryColor) => {
  // 1. 根据主题色生成色值表
  const colors = generateColor(primaryColor)
  // 2.获取当前 element-plus 的默认样式表，并且把需要进行替换的色值打上标记
  let cssText = await getElementPlusStyle()
  // 3.遍历生成的色值表，在默认样式表进行全局替换；
  Object.keys(colors).forEach((key) => {
    cssText = cssText.replace(
      new RegExp('(:|\\s+)' + key, 'g'),
      '$1' + colors[key]
    )
  })
  return cssText
}

// 根据主题色，生成色值表
export const generateColor = (primary) => {
  if (!primary) return
  const colors = { primary }
  Object.keys(formula).forEach((key) => {
    const value = formula[key].replace(/primary/g, primary)
    colors[key] = '#' + rgbHex(color.convert(value))
  })
  return colors
}

// https://unpkg.com/element-plus@2.2.2/dist/index.css
// 获取element-plus 样式表
export const getElementPlusStyle = async () => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const version = require('element-plus/package.json').version
  const { data } = await axios.get(
    `https://unpkg.com/element-plus@${version}/dist/index.css`
  )
  return markElementPlusStyle(data)
}

// 把获取的element-plus样式表，里面的变量打上标记
export const markElementPlusStyle = (data) => {
  // element-plus 默认色值
  const colorMap = {
    '#3a8ee6': 'shade-1',
    '#409eff': 'primary',
    '#53a8ff': 'light-1',
    '#66b1ff': 'light-2',
    '#79bbff': 'light-3',
    '#8cc5ff': 'light-4',
    '#a0cfff': 'light-5',
    '#b3d8ff': 'light-6',
    '#c6e2ff': 'light-7',
    '#d9ecff': 'light-8',
    '#ecf5ff': 'light-9'
  }
  Object.keys(colorMap).forEach(
    (key) => (data = data.replace(new RegExp(key, 'ig'), colorMap[key]))
  )
  return data
}
