import axios from 'axios'
import { useUserStore } from '@/store/index.ts'

const service = axios.create({
  baseURL: 'http://localhost:3000/',
  timeout: 30 * 60
})

service.interceptors.request.use(
  (config) => {
    const userStore = useUserStore()
    if (userStore.token) {
      config.headers.Authorization = `JIUXUAN ${userStore.token}`
    }
    return config
  },
  (error) => {
    return error
  }
)

service.interceptors.response.use(
  (response) => {
    return response.data
  },
  (error) => {
    return error
  }
)

export const post = async (url, params) => {
  return service.post(url, params)
}

export const get = async (url, params) => {
  return service.get(url, params)
}
