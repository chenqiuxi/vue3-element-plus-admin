import i18n from '@/i18n'

// 登录页面的国际化
export const i18nLogin = (message: string) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore：
  return i18n.global.t(`${message}`)
}

// 全局配置
export const i18nGlobal = (message: string) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore：
  return i18n.global.t(`${message}`)
}
