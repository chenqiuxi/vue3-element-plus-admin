import dayjs from 'dayjs'

// 时间格式化
const dateFilter = (val: any, format = 'YYYY-MM-DD'): string => {
  if (!isNaN(val)) {
    val = parseInt(val)
  }
  return dayjs(val).format(format)
}

export default (app) => {
  app.config.globalProperties.$filters = {
    dateFilter
  }
}
