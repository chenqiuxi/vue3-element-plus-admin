// 此处不要导入 @/i18n 使用 i18n.global ，因为我们在 router 中 layout 不是按需加载，所以会在 Guide 会在 I18n 初始化完成之前被直接调用。导致 i18n 为 undefined
const steps = () => {
  return [
    {
      element: '#guide-start',
      popover: {
        title: '引导',
        description: '描述',
        position: 'bottom-right'
      }
    },
    {
      element: '#guide-menu',
      popover: {
        title: '左侧菜单',
        description: '左侧菜单描述',
        position: 'bottom-right'
      }
    }
  ]
}
export default steps
