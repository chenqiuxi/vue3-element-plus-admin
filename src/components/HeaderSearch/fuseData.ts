import { RouteRecordRaw } from 'vue-router'
import path from 'path'
// import { i18nGlobal } from '@/utils/i18n.utils'
/**
 * 筛选出可供搜索的路由对象
 * @param routes 路由表
 * @param basePath 基础路由，默认为 /
 * @param prefixTitle
 */
export const generateRoutes = (
  routes: RouteRecordRaw[],
  basePath = '/',
  prefixTitle: string[] = []
) => {
  // 创建result数据
  let result = []
  // 遍历路由表
  for (const route of routes) {
    const data: any = {
      path: path.resolve(basePath, route.path),
      title: [...prefixTitle]
    }
    // 当前存在meta时，使用i18n解析国际化数据，组合成新的title内容
    // 动态路由不允许被搜搜
    // 匹配动态路由的正则
    const re = /.*\/:.*/
    if (route.meta && route.meta.title && !re.exec(route.path)) {
      // const i18nTitle = i18nGlobal(`msg.route.${route.meta.title}`)
      // const i18nTitle = route.meta.title
      data.title = [...data.title, route.meta.title]
      result.push(data)
    }

    // 存在children，则递归
    if (route.children) {
      const tempRoutes = generateRoutes(route.children, data.path, data.title)
      if (tempRoutes.length >= 1) {
        result = [...result, ...tempRoutes]
      }
    }
  }

  return result
}
