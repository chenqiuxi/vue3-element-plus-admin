// interface FormItemTypes {
//   type?: string
//   label?: string
//   prop?: string
//   attrs?: {
//     clearable?: boolean
//     disabled?: boolean
//   }
//   rules:[]
// }

// eslint-disable-next-line @typescript-eslint/no-namespace
export declare namespace Form {
  type ItemType =
    | 'password'
    | 'text'
    | 'textarea'
    | 'radio'
    | 'checkbox'
    | 'select'
}

// {
//     type: 'input',
//     label: '用户名：',
//     prop: 'username',
//     attrs: {
//       clearable: true
//     },
// ;[
//   {
//     required: true,
//     message: '用户名不能为空',
//     trigger: 'blur'
//   },
//   {
//     min: 3,
//     max: 5,
//     message: '不少于三个字符，不多于5个字符',
//     trigger: 'blur'
//   }
// ]
// },
