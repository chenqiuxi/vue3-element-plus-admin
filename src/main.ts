import { createApp } from 'vue'
import router from './router'
import i18n from './i18n'
import './permission' // 路由鉴权
import App from './App.vue'

import installPlugin from '@/plugins' // elemet、pinia、v-md-editor 等插件use

const app = createApp(App)
installPlugin(app)

app.use(i18n).use(router).mount('#app')
