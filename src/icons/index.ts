import SvgIcon from '@/components/SvgIcon/index.vue'

//  1.导入所有的svg图标,require.context返回一个函数，可以通过svgRequire.keys()获取所有的图表
const svgRequire = require.context('./svg', false, /\.svg$/)
svgRequire.keys().forEach((svg) => svgRequire(svg))
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default (app: any) => app.component('svg-icon', SvgIcon)
