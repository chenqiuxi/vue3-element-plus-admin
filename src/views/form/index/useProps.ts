import { reactive } from 'vue'

export const options = reactive([
  {
    type: 'input',
    label: '用户名：',
    prop: 'username',
    attrs: {
      clearable: true
    },
    rules: [
      {
        required: true,
        message: '用户名不能为空',
        trigger: 'blur'
      },
      {
        min: 3,
        max: 5,
        message: '不少于三个字符，不多于5个字符',
        trigger: 'blur'
      }
    ]
  },
  { type: 'input', label: '密码：', prop: 'password' },
  {
    type: 'select',
    label: '选择城市：',
    prop: 'city',
    attrs: {
      multiple: true,
      collapseTags: true
    },
    options: [
      { value: '0', label: 'Option1' },
      { value: '1', label: 'Option2' }
    ]
  },
  {
    type: 'checkbox',
    label: '复选框',
    prop: 'checkbox',
    options: [
      { label: 'Option A' },
      { label: 'Option B' },
      { label: 'Option C' }
    ]
  },
  {
    type: 'radio',
    label: '单选框组',
    prop: 'sex',
    options: [{ label: '男性' }, { label: '女性' }]
  },
  {
    type: 'submit',
    options: [
      { event: 'submit', text: '提交', attrs: { type: 'primary' } },
      { event: 'reset', text: '重置表单' }
    ]
  }
])
