// import { ref } from 'vue'
import Sortable from 'sortablejs'

export const initSortable = (el) => {
  // 1.要拖拽的元素

  // // 2.配置拖拽对象
  Sortable.create(el, {
    // 拖拽时的类名
    ghostClass: 'sortable-ghost',
    // 拖拽结束的回调方法
    onEnd: () => console.log('拖拽表格结束')
  })
}
