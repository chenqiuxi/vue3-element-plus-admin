const baseProps = [
  { label: '出差人', prop: 'name', minWidth: 100, align: 'center' },
  { label: '出差天数', prop: 'day', minWidth: 100, align: 'center' },
  { label: '城市', prop: 'city', minWidth: 100, align: 'center' },
  { label: '性别', prop: 'sex', minWidth: 100, align: 'center' },
  { label: '年龄', prop: 'age', minWidth: 100, align: 'center' }
]

export default () => {
  const result = [...baseProps]
  return result
}
