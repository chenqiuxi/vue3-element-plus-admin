const baseProps = [
  { label: '出差人', prop: 'name', minWidth: 100, align: 'center' },
  { label: '出差天数', prop: 'day', minWidth: 100, align: 'center' }
]

export default (actionFn) => {
  const result = [
    ...baseProps,
    {
      label: '操作栏',
      prop: 'action',
      minWidth: 120,
      align: 'center',
      render: (h, row) => actionFn(h, row)
    }
  ]
  return result
}
