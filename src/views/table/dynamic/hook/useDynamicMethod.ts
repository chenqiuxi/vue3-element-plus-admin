import { ref, watch } from 'vue'
import { ElButton } from 'element-plus'
import useProps from './useProps'

// 表格操作
const actionFn = (h, row) => {
  return h('div', {}, [
    h(
      ElButton,
      {
        type: 'primary',
        size: 'small',
        onClick: () => alert(JSON.stringify(row))
      },
      '查看'
    )
  ])
}

// 初始数据
export const dynamicData = ref(useProps(actionFn))

// 复选框
export const selectDynamicLabel = ref([])

// 默认全部勾选
const initSelectDynamicLabel = () => {
  selectDynamicLabel.value = dynamicData.value.map((item) => item.label)
}
initSelectDynamicLabel()

export const tableColumns = ref([])
// 监听选中项的变化，根据选中项动态渲染表格列
watch(
  selectDynamicLabel,
  (val) => {
    const selectData = dynamicData.value.filter((item) =>
      val.includes(item.label)
    )
    tableColumns.value = selectData
  },
  { immediate: true }
)
