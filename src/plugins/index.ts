import ElementPlus from './modules/element'
import store from './modules/pinia'
import VMdEditor from './modules/v-md-editor'

import installIcons from '@/icons' // 导入svgIcon
import installDirective from '@/directives' // 自定义指令
import installFilter from '@/utils/filter.utils'

export default (app) => {
  app.use(ElementPlus).use(store).use(VMdEditor)
  installIcons(app)
  installDirective(app)
  installFilter(app)
}
