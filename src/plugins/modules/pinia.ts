import { createPinia } from 'pinia'
export * from '@/store/modules/useAppStore'
export * from '@/store/modules/useUserStore'

const store = createPinia()
export default store
