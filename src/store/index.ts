import { createPinia } from 'pinia'
export * from './modules/useAppStore'
export * from './modules/useUserStore'

export const pinia = createPinia()
