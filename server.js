// server.js
const path = require('path')
const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router(path.join(__dirname, './__mock__/db.json'))
const middlewares = jsonServer.defaults()

server.use(middlewares)

// 重写路由路径
server.use(
  jsonServer.rewriter({
    '/sys/profile': '/sys_profile',
    '/sys/logout': '/sys_logout',
    '/user/feature': '/user_feature',
    '/user/chapter': '/user_chapter',
    '/user/manage/list': '/user_manage_list',
    '/user/detail/:id': '/user_detail',
    '/role/list': '/role_list',
    '/permission/list': '/permission_list'
    // '/user/login/:id': '/user_login/:id', // 支持动态传参，不写默认是不可以传的
    // '/user/logout': '/user_logout',
    // '/posts/:category': '/posts?category=:category' // 参数写法
  })
)

// 要处理 POST、PUT 和 PATCH，您需要使用 body-parser
server.use(jsonServer.bodyParser)

// 写在自定义路由之前
server.post('/sys/login', (req, res) => {
  const { username, password } = req.body
  if (username === 'super-admin' && password === '123456') {
    res.jsonp({
      code: 0,
      message: 'success',
      token: 'aaaaaa-bbbbbb-cccccc1',
      role: [
        { id: 1, title: '超级管理员' },
        { id: 2, title: '管理员' }
      ],
      buttons: ['importExcel']
    })
  } else {
    res.jsonp({
      code: 1,
      message: '用户名密码错误！'
    })
  }
})

server.use(router)
server.listen(3000, () => {
  console.log('server api is running port 30001')
})
